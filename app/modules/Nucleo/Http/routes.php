<?php

Route::group(['prefix' => 'nucleo', 'namespace' => 'Modules\Nucleo\Http\Controllers'], function()
{
	Route::get('/', 'NucleoController@index');
});