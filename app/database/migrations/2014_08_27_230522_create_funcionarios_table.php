<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFuncionariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('funcionarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('chapa', 6);
			$table->string('nome');
			$table->string('cpf', 15)->unique();
			$table->date('dataadmissao');
			$table->string('secao', 16);
			$table->string('descricaosecao');
			$table->string('situacao', 80);
			$table->string('tipo', 20);
			$table->string('funcao');
			$table->string('email');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('funcionarios');
	}

}
